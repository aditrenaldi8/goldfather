<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailmodel extends CI_Model {
	function __construct(){
		parent::__construct();
	}


	function loadBodyMail($fulname='',
		$phone='',
		$email='',
		$orderId='',
		$unit,
		$price=0,
		$total_amount=0,
		$package ='',
		$date= '',
		$bateraiPrice =0,
		$keterangan2 = 0
		){
		
		if($keterangan2 == 0){
			$html = $this->htmlemail();
		}else{
			$html = $this->htmlemail_klien();
		}
		$html = str_replace('{fulname}', $fulname, $html);
		$html = str_replace('{phone}', $phone, $html);
		$html = str_replace('{email}', $email, $html);

		$html = str_replace('{orderId}', $orderId, $html);
		$html = str_replace('{unit}', $unit, $html);
		// $html = str_replace('{keterangan}', $keterangan, $html);
		$tb = "";

		$tb.="<tr>";
		$tb.="<th scope='row'>1</th>";
		$tb.="<td>".$package."</td>";
		$tb.="<td align='center'>".date('Y-m-d',strtotime($date))."</td>";
		// $tb.="<td align='center'>".date('Y-m-d',strtotime($dataPesan->end_date))."</td>";
		// $tb.="<td>".$dataPesan->day_count." Hari</td>";
		$tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$price,0)."</span></td>";
		$tb.="</tr>";
		
		$tb.="<tr>";
		$tb.="<th scope='row'></th>";
		$tb.="<td colspan='2'><span class='pull-right'><strong>Baterai </strong>(Per-unit Rp. 50.000)</span></td>";
		$tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$bateraiPrice,0)."</span></td>";
		$tb.="</tr>";
		// $tb.="<tr>";
		// $tb.="<th scope='row'></th>";
		// $tb.="<td colspan='4'><span class='pull-right'>Administrasi</span></td>";
		// $tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$dataPesan->admin_fee,0)."</span></td>";
		// $tb.="</tr>";
		// $tb.="<tr>";
		// $tb.="<th scope='row'></th>";
		// $tb.="<td colspan='4'><span class='pull-right'>Discount</span></td>";
		// $tb.="<td align='right'><span class='pull-right'>Rp. ".number_format((int)$dataPesan->voucher_amount,0)."</span></td>";
		// $tb.="</tr>";
		$tb.="<tr>";
		$tb.="<th scope='row'></th>";
		$tb.="<td colspan='2'><span class='pull-right'>Total</span></td>";

		$tb.="<td align='right'><span class='pull-right'><strong>Rp. ".number_format($total_amount,0)."</strong></span></td>";
		$tb.="</tr>";
		$html = str_replace('{detailpemesanan}', $tb, $html);

		return $html;
	}
	
	private function htmlemail(){
		return<<<HTML

<html>
	<head>
	<style>
	body {
		font-family: arial, sans-serif;
		font-size:14px;
		margin:20px;
	}
	
	</style>
	</head>
	<body>
		<span>Dear <b>Admin GoldFather Studio</b>,</span>
		<br/>
		<p> Berikut Detail Pemesanan Jasa GoldFather Studio</p>

		<p>Keterangan Pemesanan<br><br>
		<b>Order id:</b> {orderId}<br>
		<!-- <b>Jumlah:</b> {unit} Unit<br> -->
		<b>Status:</b> Menunggu Konfirmasi<br><br>

		Harap segera menghubungi customer bersangkutan untuk melakukan konfirmasi lebih lanjut<br><br>

		<!-- <b>{keterangan}</b> -->
		<br><br>
		</p>
		<table width="100%" style="border-collapse: collapse;font-size:12px" border="1" cellpadding="4" cellspacing="2">
				  <thead>
				    <tr>
				      <th>#</th>
				      <th>Paket</th>
				      <th>Tanggal Penyewaan Jasa</th>
				      <!-- <th>Tanggal Kepulangan</th>
				      <th>Days</th> -->
				      <th>Harga</th>
				    </tr>
				  </thead>
				  <tbody>
				   {detailpemesanan}
				  </tbody>
		</table>
		<br><br>
		<span><b>Informasi Pelanggan</b></span>
		<br>
		<table width="100%" style="font-size:14px">
			<tr>
				<td width="20%">Nama Pelanggan</td>
				<td width="80%">: <b>{fulname}</b></td>
			</tr>
			<tr>
				<td width="20%">Kontak Pelanggan</td>
				<td width="80%">: <b>{phone}</b></td>
			</tr>
			<tr>
				<td width="20%">Email Pelanggan</td>
				<td width="80%">: <b>{email}</b></td>
			</tr>
			
		</table>
		<br><br>
		<!-- <b>Metode Pengiriman:</b> 
		<p>{fullAdress1}</p>
		<b>Metode Pengembalian:</b> 
		<p>{fullAdress2}</p>
		<br/> -->
	
		<br><br>
		<b>*Note : 
			<p>1. Silahkan Menghubungi customer bersangkutan untuk melakukan konfirmasi </p>
		</b>
		<br><br>

		<!-- <span>Thanks!<br/>
		The Passpod Team</span>
		<br><br>
		<img src="http://passpod.id/assets/images/logo_passpod.png" alt="Passpod.id|LAYANAN TERBAIK BUAT ANDA" width="150px">
		<br><br>
		<table width="100%" style="font-size:11px">
			<tr>
				<td>Hotline</td>
				<td>: +62 812 8 171819</td>
			</tr>
			<tr>
				<td>Whatsapp</td>
				<td>: +62 888 1 171819</td>
			</tr>
			<tr>
				<td>Fixed line</td>
				<td>: +6221 63850731</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>: support[at]passpod.id</td>
			</tr>
			<tr>
				<td>Facebook</td>
				<td><a href="https://www.facebook.com/Passpod-724533597698946/" title="FB Passpod">www.facebook.com/passpod</a></td>
			</tr>
			<tr>
				<td>Instagram</td>
				<td><a href="https://www.instagram.com/passpod_wifi/" title="Instaagram Passpod">www.instagram.com/passpod_wifi</a></td>
			</tr>
			<tr>
				<td>Alamat Kantor</td>
				<td>Jl. K.H.Hasyim Ashari Ruko Roxy Mas Blok C 2 No.37 Jakarta Pusat 10150</td>
			</tr>
		</table> -->
		</p> 
	</body>
</html>
		
HTML;
		
	}
		
	private function htmlemail_klien(){
		return<<<HTML
		
<html>
	<head>
	<style>
	body {
		font-family: arial, sans-serif;
		font-size:14px;
		margin:20px;
	}
				
	</style>
	</head>
	<body>
		<span>Dear <b>{fulname}</b>,</span>
		<br/>
		<p>Terima kasih untuk pemesanan Anda di GoldFather Studio.</p>
		<p>Keterangan Pemesanan<br><br>
		<b>Order id:</b> {orderId}<br>
		<!-- <b>Jumlah:</b> {unit} Unit<br> -->

		Tim Kami Akan segera menghubungi Anda untuk detail pemesanan dan penentuan waktu untuk melakukan survey lapangan<br><br>
		<!-- <b>{keterangan}</b> -->
		<br><br>
		</p>
		<table width="100%" style="border-collapse: collapse;font-size:12px" border="1" cellpadding="4" cellspacing="2">
				  <thead>
				    <tr>
				      <th>#</th>
				      <th>Paket</th>
				      <th>Tanggal Penyewaan Jasa</th>
				      <!-- <th>Tanggal Kepulangan</th>
				      <th>Days</th> -->
				      <th>Harga</th>
				    </tr>
				  </thead>
				  <tbody>
				   {detailpemesanan}
				  </tbody>
		</table>
		<br><br>
		<span><b>Informasi Pelanggan</b></span>
		<br>
		<table width="100%" style="font-size:14px">
			<tr>
				<td width="20%">Nama Pelanggan</td>
				<td width="80%">: <b>{fulname}</b></td>
			</tr>
			<tr>
				<td width="20%">Kontak Pelanggan</td>
				<td width="80%">: <b>{phone}</b></td>
			</tr>
			<tr>
				<td width="20%">Email Pelanggan</td>
				<td width="80%">: <b>{email}</b></td>
			</tr>
				
		</table>
		<br>
		<b>*Note :
			<p>1. Apabila ada informasi pemesanan yang salah, Silahkan hubungi Customer Service Kami (Nomor Hape DC)</p>
		</b>
		<br><br>
				
		<!-- <span>Thanks!<br/>
		The Passpod Team</span>
		<br><br>
		<img src="http://passpod.id/assets/images/logo_passpod.png" alt="Passpod.id|LAYANAN TERBAIK BUAT ANDA" width="150px">
		<br><br>
		<table width="100%" style="font-size:11px">
			<tr>
				<td>Hotline</td>
				<td>: +62 812 8 171819</td>
			</tr>
			<tr>
				<td>Whatsapp</td>
				<td>: +62 888 1 171819</td>
			</tr>
			<tr>
				<td>Fixed line</td>
				<td>: +6221 63850731</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>: support[at]passpod.id</td>
			</tr>
			<tr>
				<td>Facebook</td>
				<td><a href="https://www.facebook.com/Passpod-724533597698946/" title="FB Passpod">www.facebook.com/passpod</a></td>
			</tr>
			<tr>
				<td>Instagram</td>
				<td><a href="https://www.instagram.com/passpod_wifi/" title="Instaagram Passpod">www.instagram.com/passpod_wifi</a></td>
			</tr>
			<tr>
				<td>Alamat Kantor</td>
				<td>Jl. K.H.Hasyim Ashari Ruko Roxy Mas Blok C 2 No.37 Jakarta Pusat 10150</td>
			</tr>
		</table> -->
		</p>
	</body>
</html>
				
HTML;
		
	}
		
}