<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
	function __Construct(){
		parent::__Construct();
		$this->load->model('Mainmodel');
		$this->load->model('Emailmodel');
		$this->load->library('swiftmailer');
	}
	
	function package($ID){		
		$this->booking($ID);
	}
	
	function index(){
		if($this->input->post('action')=='getpesanan'){
			$this->getpesanan();
			exit;
		} else if($this->input->post('action')=='verify'){
			$this->verifydata();
			exit;
		}else if($this->input->post('action')=='verifydate'){
			$this->verifydate();
			exit;
		} else {
			$this->booking();
		}
	}
	
	function verifydate(){
		$status='ok';
		$pesan='';
		
		if($this->input->post('tanggalbrkt')){
			$date = strtotime(gmdate('Y-m-d H:i:s',time()+25200));
			$start_date = date('Y-m-d', strtotime($this->input->post('tanggalbrkt')));
			$date_brkt = strtotime($start_date);
			/* mengambil selisih jam */
			$diff = ($date_brkt - $date)/3600;
			if($diff < 36 ){
				$status='failed';
				if($pesan) $pesan.="\n";
				$pesan.='Pemesanan Hanya Bisa Dilakukan Selambat-lambatnya 2 Hari Sebelum Pemakaian';
				$pesan.="\n";
				$pesan.='Untuk Pemesanan Urgent, Hubungi Customer Service Kami';
			}
		}
	}

	function pay(){
		$order_date=$this->Mainmodel->sekarang();
		$date1 = $this->input->post('tanggal');
		$package_id =  $this->input->post('package');
		// $date2 = $this->input->post('tanggalkembali');
		$date = date('Y-m-d', strtotime($date1));
		// $end_date = date('Y-m-d', strtotime($date2));
		// $diff=date_diff(date_create($start_date),date_create($end_date));
		// $day_count=$diff->format('%a');
		// if($day_count<0) $day_count=0;
		// $day_count=(int) ($day_count+1);

		$additional_battery=(int) $this->input->post('baterai');
		$sql="SELECT * FROM tb_package WHERE id=".$this->db->escape($package_id);
		$que=$this->Mainmodel->kueri($sql);
		$tarif=0;

		foreach($que->result() as $res){
			$package = $res->package_type;
			$tarif=(float) $res->price;
			$detail = $res->detail;
			$id = $res->id;
		}

		$total_price = ($tarif + (50000 * $additional_battery));
		// $payment_type = $this->input->post('payment_type');
		$order_id = "GFT".substr(md5($order_date),0,4);
		
		// klien
		$email=strtolower($this->input->post('email'));
		$sql="SELECT * FROM tb_klien WHERE email=".$this->db->escape($email);
		$que=$this->Mainmodel->kueri($sql);
		$num = $que->num_rows();
		$name=$this->input->post('name');
		$name = addslashes($name);
	
		$name=str_replace("&","dan", $name);
		$name=str_replace("<","", $name);
		$name=str_replace(">","", $name);
	
		$phone=$this->input->post('phone');
		$data_klien=compact('name','email','phone');
		if($num){
			$this->Mainmodel->Update('tb_klien',$data_klien,"email='{$email}'");
			foreach($que->result_array() as $res){
				$data_klien=$res;
				$klien_id=$res['id'];
				break;
			}
		} else {
			$klien_id=$this->Mainmodel->Insert('tb_klien',$data_klien);			
		}
		
		$data_order=compact('order_id','package_id','klien_id','additional_battery','detail','total_price','date','order_date');
		$o_id=$this->Mainmodel->Insert('tb_order',$data_order);

		$this->proses_email_cust($klien_id, $o_id, $package, $tarif);
		$this->proses_email_support($klien_id, $o_id, $package, $tarif);

		$this->load->view('selesaiorder');
	}

	
	private function verifydata(){
		$status='ok';
		$pesan='';
		if($this->input->post('email')){
			if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
				$status='failed';
				if($pesan) $pesan.="\n";
				$pesan.='Format email salah.';
			}
		} else {
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Email harus diisi.';
		}
		if($this->input->post('package') == ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Paket harus dipilih.';
		}
		if($this->input->post('baterai') < 0){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Input Data Tambah Baterai Salah.';
		}
		if(!$this->input->post('tanggal') || $this->input->post('tanggal')== ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Tanggal Mulai Digunakan harus diisi.';
		}
		if($this->input->post('name') == ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Nama harus diisi.';
		}
		if($this->input->post('phone') == ""){
			$status='failed';
			if($pesan) $pesan.="\n";
			$pesan.='Nomor Hp harus diisi.';
		}
		
		echo json_encode(compact('status','pesan','tgl1','tgl2'));
	}

	private function booking($package_id=0){
	
		$sql="SELECT * FROM tb_package WHERE 1 ";
		$tarifs=$this->Mainmodel->kueri($sql);
		
		$sql="SELECT * FROM tb_payment_type ORDER BY id";
		$banks=$this->Mainmodel->kueri($sql);
		
		$this->load->view('booking',compact('tarifs', 'banks', 'package_id'));
	}
	
	private function getpesanan(){	
		$_totalamount=0;
		$sql="SELECT * FROM tb_package WHERE id=".$this->input->post('c');
		$que=$this->Mainmodel->kueri($sql);
		$tarifs=array();
		foreach($que->result_array() as $res){
			$tarifs=$res;
			break;
		}
	
		$paket=$tarifs['package_type'];
		$baterai=$this->input->post('j');
		$date1=date_create($this->input->post('s'));
		
		$_rentalamount = $tarifs['price'];
		$unitrental='Paket'. $paket;
		$_totalamount+=$_rentalamount;
		$rentalamount='IDR '.number_format($_rentalamount,0,',','.');
			
		$tanggal = date_format($date1,'d-F-Y');
	
		$unitbaterai=$baterai.' unit x '.number_format(50000,0,',','.');
		$_bateraifee = 50000 * $baterai;
		$_totalamount+=$_bateraifee;
		$bateraifee='IDR '.number_format($_bateraifee,0,',','.');
		$totalamount='IDR '.number_format($_totalamount,0,',','.');
		echo json_encode(compact('paket','rentalamount','unitrental','tanggal','unitbaterai','totalamount','bateraifee'));
	}

	private function ubahtanggal($date){
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
		
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
		return($result);
	}

	private function proses_email_cust($klien_id, $order_id, $package, $tarif){
		$res_klien = $this->get_data_klien($klien_id);
		$res_order = $this->get_data_order($order_id);
		
		$body = $this->Emailmodel->loadBodyMail(
			$res_klien->name, 
			$res_klien->phone, 
			$res_klien->email,
			$res_order->order_id,
			$res_order->additional_battery,
			$tarif,
			$res_order->total_price,
			$package,
			$res_order->date,
			(50000*$res_order->additional_battery),
			1	
		);	

		$result=1;
		$result = $this->swiftmailer->sendMail($res_klien->email,'GoldFather Studio - Konfirmasi Pemesanan', $body);
		if($result){
			$email_sent_cst=$this->Mainmodel->sekarang();
			$this->Mainmodel->Update('tb_order',compact('email_sent_cst'),"id={$order_id}");	
			return 1;
		} else {
			return 0;	
		}
	}

	private function proses_email_support($klien_id, $order_id, $package, $tarif){
		$res_klien = $this->get_data_klien($klien_id);
		$res_order = $this->get_data_order($order_id);
		
		$body = $this->Emailmodel->loadBodyMail(
			$res_klien->name, 
			$res_klien->phone, 
			$res_klien->email,
			$res_order->order_id,
			$res_order->additional_battery,
			$tarif,
			$res_order->total_price,
			$package,
			$res_order->date,
			(50000*$res_order->additional_battery),
			0	
		);	

		// var_dump($body);die;
		
		$result=1;
		$result = $this->swiftmailer->sendMail('goldfatherstudio@gmail.com','GoldFather Studio - Konfirmasi Pemesanan', $body);
		if($result){
			$email_sent_sys=$this->Mainmodel->sekarang();
			$this->Mainmodel->Update('tb_order',compact('email_sent_sys'),"id={$order_id}");	
			return 1;
		} else {
			return 0;	
		}
	}

	private function get_data_klien($ID){
		$sql="SELECT * FROM tb_klien WHERE id={$ID}";
		$que_klien=$this->Mainmodel->kueri($sql);
		foreach($que_klien->result() as $res_klien){
			return $res_klien;
			break;
		}
	}

	private function get_data_order($ID){
		$sql="SELECT * FROM tb_order WHERE id={$ID}";
		$que_order=$this->Mainmodel->kueri($sql);
		foreach($que_order->result() as $res_order){
			return $res_order;
			break;
		}
	}
}