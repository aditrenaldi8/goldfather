<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
?>

	 <div id="booking" class="booking">
        <div class="container content-lg">
            <div class="row text-center margin-b-40">
                <div class="col-sm-6 col-sm-offset-3">
                    <h2>Booking GoldFather</h2>
                </div>
            </div>
            <!--// end row -->

            <div class="row">
               <section class="booking-form">
               		<div class="col-md-6">
						<form id="form-sewa" class="form-group" method="post" action="<?php echo base_url(); ?>booking/pay" class="range text-left">     
						<!-- RD Mailform-->
							<div>
								<label for="country" class="form-label">Paket</label>
								<select id="package" name="package" class="form-control">
									<option value="">Pilih Paket</option>
									<?php foreach($tarifs->result() as $data){ ?>
										<option value="<?php echo $data->id; ?>"<?php echo ($package_id==$data->id)?' selected':''; ?>><?php echo $data->package_type; ?></option>
										<?php } ?>
								</select>
							</div>
							<div>
								<label for="jumlah-modem" class="form-label">Tambah Baterai</label>
								<input id="baterai" class="form-control" type="number" value="0" min="0" name="baterai" data-constraints="@Required @Integer" class="form-control form-control-gray">
							</div>
							<div>
								<label for="tanggal" class="form-label">Tanggal Penyewaan</label>
								<input id="tanggal" class="form-control" type="date" name="tanggal" data-constraints="@Required" class="tanggal form-control form-control-gray">
							</div>
							<div>
								<label for="first-name" class="form-label form-label-outside">Nama Pemesan</label>
								<input id="name" type="text" name="name" data-constraints="@Required" class="form-control form-control-gray">
							</div>
							<div>
								<label for="contact-email" class="form-label">E-mail</label>
								<input id="contact-email" class="form-control" type="email" name="email" data-constraints="@Required @Email" class="form-control form-control-gray">
							</div>
							<div>
								<label for="contact-phone" class="form-label ">No Whatsapp <span style="font-size:10px;color:red">( jika tidak ada gunakan No HP )</span></label>
								<input id="contact-phone" class="form-control" type="text" name="phone" data-constraints="@Required" class="form-control form-control-gray">
							</div>
							<!-- <div >
								<label for="country" class="form-label">Metode Pembayaran</label>
								
									<select id="payment_type" name="payment_type" class="form-control form-control-gray selectpicker">
										<option value="">Pilih Metode Pembayaran</option>

										<?php foreach($banks->result() as $data){ ?>
											<option value="<?php echo $data->id?>"> Bank Transfer <?php echo $data->bank?></option>
										<?php } ?>
									</select>
								
							</div>	 -->
						</form>	
					</div>	
			
					<div class="col-md-6">
						<div>
							<br>
							<h4 class="text-bold text-capitalize">Pesanan anda</h4>
							<hr class="divider divider-primary divider-80 divider-offset">
							<table  width="100%" class="pesanan-anda">
								<tr>
									<td width="50%"><b>Tipe Paket</b></td>
									<td width="50%" class="paket kanan"><b>Belum Dipilih</b></td>
								</tr>
								<tr>
									<td colspan="2" class="sum-divider">&nbsp;</td>
								</tr>
								<tr>
									<td><b>Tanggal Penyewaan </b></td>
									<td class="tanggal kanan"></td>
								</tr>
								<tr>
									<td colspan="2" class="sum-divider">&nbsp;</td>
								</tr>
								<tr>
									<td><b>Biaya Sewa</b><br />
									<span class="unit-rental"></span></td>
									<td class="rental-amount kanan">IDR 0</td>
								</tr>
								<tr>
									<td colspan="2" class="sum-divider">&nbsp;</td>
								</tr>
	
								<tr>
									<td><b>Biaya Tambah Baterai</b><br />
									<span class="unit-baterai-fee"></span></td>
									<td class="baterai-fee kanan">IDR 0</td>
								</tr>
								<tr>
									<td colspan="2" class="sum-divider">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2" class="sum-divider">&nbsp;</td>
								</tr>
								<tr>
									<td><b>Jumlah yang Harus Dibayar</b></td>
									<td class="total-amount kanan" style="font-weight: bold; font-size: 1.1em">IDR 0</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="col-md-12">
						<br>
						<div>
							<button style="min-width: 140px;" type="button" class="btn btn-primary btn-md button-bayar">Bayar</button>
						</div>
					</div>
				</section>
            </div>
            <!--// end row -->
        </div>
    </div>

<?php
$this->load->view('footer');
?>
<!-- <script src="<?php echo base_url(); ?>assets/js/datepicker.js"></script> -->
<script>
	
	var tstartdate='';
	function cekpesanan(){
		var country,periode,jumlah,voucher,tbrkt,tkembali;
		package=$(".booking-form form select[name='package']").val();
		tanggal=$(".booking-form form input[name='tanggal']").val();
		baterai=$(".booking-form form input[name='baterai']").val();
		if(package!="" && baterai){
			$.post( '<?php echo base_url(); ?>/booking', { action: 'getpesanan', c: package,s: tstartdate, j: baterai , },function(data){
				$('.pesanan-anda .paket').text(data.paket);
				$('.pesanan-anda .unit-rental').text(data.unitrental);
				$('.pesanan-anda .rental-amount').text(data.rentalamount);
				$('.pesanan-anda .tanggal').text(data.tanggal);
				$('.pesanan-anda .unit-baterai-fee').text(data.unitbaterai);
				$('.pesanan-anda .baterai-fee').text(data.bateraifee);
				$('.pesanan-anda .total-amount').text(data.totalamount);
				
			},"json");
		}
	}

	function cektanggal(){
		var date = document.getElementById("tanggal").value;
		var myDate = new Date(date);
		var today = new Date();
		today.setHours(0,0,0,0);
		myDate.setHours(0,0,0,0);

		if(myDate < today) {	
			alert("Tanggal Sudah Terlewat!");
			$("#tanggalkembali").prop("disabled",true);
			document.getElementById("tanggal").value='';
		}
	}

	function ceknomor(){
		var phoneNumber;
		phoneNumber=$(".booking-form form input[name='telepon']").val();
		var regExp = /[^0-9]/;
		if(phoneNumber.match(regExp)) {
			alert("Nomor Handphone salah");
			document.getElementById('contact-phone').value='';
			return false;
		}
		else {
			return true;
		}
	}
</script>

<script>
    $(document).ready(function() {
      $('.button-bayar').click(function(e){
			$('#form-sewa').submit();
		});

		$('#form-sewa').submit(function(e){
			e.preventDefault();	
			var postvars=$('#form-sewa').serialize()+'&action=verify';
			$.post( '<?php echo base_url(); ?>/booking', postvars ,function(data){
				if(data.status=='ok'){
					$('#form-sewa').unbind('submit').submit();
				} else {
					alert(data.pesan);
				}
			},"json");
		});

		$(".booking-form form select[name='package']").change(function(e){
			cekpesanan();
		});
		$(".booking-form form input[name='baterai']").change(function(e){
			if($(this).val()>=0) cekpesanan();
		});

		// 	$('.tanggal').datepicker({
		// 				locale: {
		// 						format: 'DD-MM-YYYY'
		// 				},
		// 		});


		$('#tanggal').change(function(){

			tstartdate = $(this).val();
			cektanggal();
			if(document.getElementById("tanggal").value != ""){
				var postvars=$('#form-sewa-modem').serialize()+'&action=verifydate';
				$.post( '<?php echo base_url(); ?>/booking', postvars ,function(data){
					if(data.status=='ok'){
						cekpesanan();
					} else {
						alert(data.pesan);
						document.getElementById("tanggal").value='';
					}
				},"json");
			}
		});


		$('#contact-phone').change(function(){
			ceknomor();
		});
	
		$('#country').change(function(){
			if (this.value == '21') {
				$(".europe").removeClass('hide');
			}else {
				$(".europe").addClass('hide');
			}
		});
    });
</script>