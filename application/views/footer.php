<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

            <!-- Copyright -->
            <div class="content container">
                <div class="row">
                    <div class="col-xs-6">
                        <img class="footer-logo" src="<?php echo base_url(); ?>assets/img/logo.png" alt="Aironepage Logo" width="40%">
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="margin-b-0"><a class="fweight-700" href="http://keenthemes.com/preview/aironepage/">Aironepage</a> Theme Powered by: <a class="fweight-700" href="http://www.keenthemes.com/">KeenThemes.com</a></p>
                    </div>
                </div>
                <!--// end row -->
            </div>
            <!-- End Copyright -->
        </footer>
        <!--========== END FOOTER ==========-->

        <!-- Back To Top -->
        <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

        <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- CORE PLUGINS -->
        <script src="<?php echo base_url(); ?>assets/vendor/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url(); ?>assets/vendor/jquery.easing.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery.back-to-top.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery.smooth-scroll.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery.wow.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/swiper/js/swiper.jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/masonry/jquery.masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/masonry/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U&amp;callback=initMap" async defer></script>

        <!-- PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/js/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/components/wow.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/components/swiper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/components/maginific-popup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/components/masonry.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/components/gmap.min.js" type="text/javascript"></script>

    </body>
</html>
