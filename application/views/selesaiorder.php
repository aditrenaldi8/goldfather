<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('header');
?>
    <div id="booking" class="promo-block">
        <div class="container content-lg">
            <div class="row text-center margin-b-40">
                <div class="col-sm-6 col-sm-offset-3">
                  <h1 style="color:white">Terima Kasih</h1>
                  <h2 style="color:white">Mohon periksa email Anda untuk informasi lebih lanjut</h2>
                </div>
            </div>  
        </div>
    </div>
<?php
$this->load->view('footer');
?>