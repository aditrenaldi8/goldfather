<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="id-ID" class="wide smoothscroll wow-animation">
  <head>
      <meta charset="utf-8"/>
      <title>GoldFather</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1" name="viewport"/>
      <meta content="" name="description"/>
      <meta content="" name="author"/>

      <!-- GLOBAL MANDATORY STYLES -->
      <link href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url(); ?>assets/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
      <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

      <!-- PAGE LEVEL PLUGIN STYLES -->
      <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/vendor/swiper/css/swiper.min.css" rel="stylesheet" type="text/css"/>
      <link href="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css"/>

      <!-- THEME STYLES -->
      <link href="<?php echo base_url(); ?>assets/css/layout.css" rel="stylesheet" type="text/css"/>
      <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>

      <!-- Favicon -->
      <link rel="shortcut icon" href="favicon.ico"/>
  </head>
  <!-- BODY -->
  <body id="body" data-spy="scroll" data-target=".header">

    <!--========== HEADER ==========-->
    <header class="header navbar-fixed-top">
        <!-- Navbar -->
        <nav class="navbar" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container js_nav-item">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                    </button>

                    <!-- Logo -->
                    <div class="logo">
                        <a class="logo-wrap" href="#body">
                            <img class="logo-img logo-img-main" src="<?php echo base_url();?>assets/img/logo.png" alt="Asentus Logo" width="40%">
                            <img class="logo-img logo-img-active" src="<?php echo base_url(); ?>assets/img/logo.png" alt="Asentus Logo">
                        </a>
                    </div>
                    <!-- End Logo -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container">
                        <ul class="nav navbar-nav navbar-nav-right">
                            <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>#body">Home</a></li>
                            <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>#contact">About Us</a></li>
                            <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>#products">Portofolio</a></li>
                            <!-- <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>#work">How to Order</a></li> -->
                            <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>#pricing">Pricing</a></li>
                            <!-- <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>#contact">Contact</a></li> -->
                            <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="<?php echo base_url(); ?>booking">Booking</a></li>
                        </ul>
                    </div>
                </div>
                <!-- End Navbar Collapse -->
            </div>
        </nav>
        <!-- Navbar -->
    </header>
    <!--========== END HEADER ==========-->
