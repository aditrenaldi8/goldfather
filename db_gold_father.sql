-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2018 at 04:41 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_gold_father`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_klien`
--

CREATE TABLE IF NOT EXISTS `tb_klien` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(14) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_klien`
--

INSERT INTO `tb_klien` (`id`, `name`, `email`, `phone`) VALUES
(2, 'Aditya Renaldi', 'adityarenaldi008@gmail.com', '081221432980'),
(3, 'Aditya Renaldi', 'aditrenaldi008@gmail.com', '08122143980'),
(4, 'Aditya Renaldi', 'aditrenaldi8@gmail.com', '081221432980'),
(5, 'ANDRI', 'andridocument@gmail.com', '0813209313222');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE IF NOT EXISTS `tb_order` (
`id` int(11) NOT NULL,
  `order_id` varchar(10) NOT NULL,
  `package_id` varchar(255) NOT NULL,
  `klien_id` int(11) NOT NULL,
  `additional_battery` int(5) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `total_price` decimal(10,0) NOT NULL,
  `date` date NOT NULL,
  `payment_type` int(2) NOT NULL,
  `email_sent_cst` datetime DEFAULT NULL,
  `email_sent_sys` datetime DEFAULT NULL,
  `order_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id`, `order_id`, `package_id`, `klien_id`, `additional_battery`, `detail`, `total_price`, `date`, `payment_type`, `email_sent_cst`, `email_sent_sys`, `order_date`) VALUES
(14, 'GFTe999', '2', 2, 2, 'Durasi ± 55 menit', '1200000', '2018-03-15', 1, '2018-03-11 14:55:37', '2018-03-11 14:55:41', '2018-03-11 14:55:33'),
(15, 'GFTdfca', '3', 2, 0, 'Durasi ± 82 menit', '1500000', '2018-03-17', 1, '2018-03-11 15:09:26', '2018-03-11 15:09:30', '2018-03-11 15:09:22'),
(16, 'GFT367f', '3', 2, 2, 'Durasi ± 82 menit', '1600000', '2018-03-16', 1, '2018-03-11 15:36:48', '2018-03-11 15:36:52', '2018-03-11 15:36:44'),
(17, 'GFT263f', '1', 5, 1, 'Durasi ± 27 menit', '550000', '2018-04-07', 1, '2018-03-21 21:24:22', '2018-03-21 21:24:27', '2018-03-21 21:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_package`
--

CREATE TABLE IF NOT EXISTS `tb_package` (
`id` int(11) NOT NULL,
  `package_type` varchar(1) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `detail` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_package`
--

INSERT INTO `tb_package` (`id`, `package_type`, `price`, `detail`) VALUES
(1, 'A', '500000', 'Durasi ± 27 menit'),
(2, 'B', '1100000', 'Durasi ± 55 menit'),
(3, 'C', '1500000', 'Durasi ± 82 menit');

-- --------------------------------------------------------

--
-- Table structure for table `tb_package_detail`
--

CREATE TABLE IF NOT EXISTS `tb_package_detail` (
`id` int(11) NOT NULL,
  `package_id` int(1) NOT NULL,
  `detail` varchar(192) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_package_detail`
--

INSERT INTO `tb_package_detail` (`id`, `package_id`, `detail`) VALUES
(1, 1, 'Durasi ± 27 menit'),
(2, 2, 'Durasi ± 55 menit'),
(3, 3, 'Durasi ± 82 menit');

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment_type`
--

CREATE TABLE IF NOT EXISTS `tb_payment_type` (
`id` int(5) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `account_number` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_payment_type`
--

INSERT INTO `tb_payment_type` (`id`, `bank`, `name`, `account_number`) VALUES
(1, 'BCA', 'Andri Dwi Cahyadi', '123456789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_klien`
--
ALTER TABLE `tb_klien`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_package`
--
ALTER TABLE `tb_package`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_package_detail`
--
ALTER TABLE `tb_package_detail`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_payment_type`
--
ALTER TABLE `tb_payment_type`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_klien`
--
ALTER TABLE `tb_klien`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_package`
--
ALTER TABLE `tb_package`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_package_detail`
--
ALTER TABLE `tb_package_detail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_payment_type`
--
ALTER TABLE `tb_payment_type`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
